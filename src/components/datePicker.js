import React, { useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

//Defino quais parâmetros quero receber para poder fazer o componente
const DateTimePicker = ({ type, buttonTitle, dateKey, setSchedule }) => {
  const [datePickerVisible, setDatePickerVisible] = useState(false);

  //atualizar o estado do modal
  const showDatePicker = () => {
    setDatePickerVisible(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisible(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      //Lógica para extrair hora e minutos
      const hour = date.getHours();
      const minute = date.getMinutes();

      //Lógica para montar hora e minuto no formato desejado
      const formattedTime = `${hour}:${minute}`;


      //Atualiza o estado da reserva com HH:mm formatada
      setSchedule((prevState) => ({
        ...prevState, 
        [dateKey]: formattedTime,
      }));

    } else {
        const formattedDate= date.toISOString().split('T')[0];
      //Atualizar Schedule, setando uma parte do Schedule, nessa caso seria a hora
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedDate,
      }));
    }
    hideDatePicker();
  };

  return (
    <View>
      <Button
        title={buttonTitle} //Isso faz com q a fução (true or false) atualize no datePickerModal para ser visível ou não 
        onPress={showDatePicker}
        color="#fff"
      />
      <DateTimePickerModal
        isVisible={datePickerVisible}
        mode={type} //Recebido da homePage
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}


        pickerComponentStyleIOS={{backgroundColor:"#fff"}}
        textColor="#000"
      />
    </View>
  );
};
export default DateTimePicker;
